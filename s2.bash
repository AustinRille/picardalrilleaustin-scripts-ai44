#!/bin/bash


TODAY=$(date +"%Y-%m-%d")
echo "Enter Last name: "
read lname
echo "Enter First name: "
read fname
echo "Your birthdate(yyyy-mm-dd)"
read birthdate
tmpDays=$( printf '%s' $(( $(date -u -d"$TODAY" +%s) - $(date -u -d"$birthdate" +%s)))  )
age=$(( $tmpDays / 60 / 60 / 24 / 364 ))
echo "Hello, $fname $lname! You're $age years old!"